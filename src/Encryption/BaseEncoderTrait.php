<?php
/**
 * Base3Encoder.php
 *
 * @copyright 2021
 * @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 */

namespace Flagstone\EncryptionBundle\Encryption;

/**
 * Trait BaseEncoderTrait
 * Contains __construct, encode and decode method to avoid repetitive same methods in all encoders class
 * @package Flagstone\EncryptionBundle\Encryption
 */
trait BaseEncoderTrait
{
    /**
     * BaseEncoderTrait constructor.
     */
    public function __construct()
    {
        $this->changeBaseString(self::BASE);
    }

    /**
     * @param   string|null $string
     * @return  string
     */
    public function encode(?string $string = ''): string
    {
        $this->testBaseLength($this->baseString, strlen(self::BASE));
        return $this->encodeString($string, self::SPLIT, self::POWER, $this->baseString, self::NB_CHAR_PER_SPLIT, self::BYTES_BLOCK_LENGTH);
    }

    /**
     * @param   string|null $string
     * @return  string
     */
    public function decode(?string $string = ''): string
    {
        return $this->decodeString($string, self::SPLIT, self::POWER, $this->baseString, self::NB_CHAR_PER_SPLIT, self::BYTES_BLOCK_LENGTH);
    }
}