<?php
/**
 * EncryptionInterface.php
 *
 * @copyright 2021
 * @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 */

namespace Flagstone\EncryptionBundle\Encryption\Interfaces;

/**
 * Interface EncryptionInterface
 * @package Flagstone\EncryptionBundle\Encryption\Interfaces
 *
 * Methods needed to encode/decode strings
 */
interface EncryptionInterface
{
    /**
     * EncryptionInterface constructor.
     * Needed,must define baseString string
     */
    public function __construct();

    /**
     * @param   string|null $string
     * @return  string
     */
    public function encode(?string $string = ""): string;

    /**
     * @param   string|null $string
     * @return  string
     */
    public function decode(?string $string = ""): string;
}