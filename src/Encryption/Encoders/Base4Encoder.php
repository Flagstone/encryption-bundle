<?php
/**
 * Base4Encoder.php
 *
 * @copyright 2021
 * @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 */

namespace Flagstone\EncryptionBundle\Encryption\Encoders;

use Flagstone\EncryptionBundle\Encryption\AbstractBaseEncoder;
use Flagstone\EncryptionBundle\Encryption\BaseEncoderTrait;

/**
 * Class Base4Encoder
 * | Code and decode a string in Base4 format.
 * | A 4 bits length string is coded in a 8 bits string (1 char). Use 4 different chars for encoding.
 * | Take 4 times more place in database
 * @package Flagstone\EncryptionBundle\Encryption\Encoders
 */
class Base4Encoder extends AbstractBaseEncoder
{
    const SPLIT = 2;                //  How many bit per char
    const POWER = 1;                //  How many time decoding/encoding
    const NB_CHAR_PER_SPLIT = 1;
    const BASE = '0123';            //  Characters to use to obtain the coded string
    const BYTES_BLOCK_LENGTH = 0;   //  Length of each block

    use BaseEncoderTrait;
}