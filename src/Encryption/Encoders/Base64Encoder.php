<?php
/**
 * Base64Encoder.php
 *
 * @copyright 2021
 * @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 */

namespace Flagstone\EncryptionBundle\Encryption\Encoders;

use Flagstone\EncryptionBundle\Encryption\AbstractBaseEncoder;
use Flagstone\EncryptionBundle\Encryption\BaseEncoderTrait;

/**
 * Class Base64Encoder
 * | Code and decode a string in Base64 format.
 * | Take 2 times more place in database, 4 chars minimum.
 * @package Flagstone\EncryptionBundle\Encryption\Encoders
 */
class Base64Encoder extends AbstractBaseEncoder
{
    const SPLIT = 6;                                                                    //  How many bit per char
    const POWER = 1;                                                                    //  How many time decoding/encoding
    const NB_CHAR_PER_SPLIT = 1;
    const BASE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';    //  Characters to use to obtain the coded string
    const BYTES_BLOCK_LENGTH = 4;                                                       //  Length of each block

    use BaseEncoderTrait;
}