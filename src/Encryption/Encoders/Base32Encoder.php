<?php
/**
 * Base32Encoder.php
 *
 * @copyright 2021
 * @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 */

namespace Flagstone\EncryptionBundle\Encryption\Encoders;

use Flagstone\EncryptionBundle\Encryption\AbstractBaseEncoder;
use Flagstone\EncryptionBundle\Encryption\BaseEncoderTrait;

/**
 * Class Base32Encoder
 * | Code and decode a string in Base32 format.
 * | Respect RFC 4648 (https://tools.ietf.org/html/rfc4648#section-5) with Extended Hex Alphabet
 * | Take 3 times more place in database
 * @package Flagstone\EncryptionBundle\Encryption\Encoders
 */
class Base32Encoder extends AbstractBaseEncoder
{
    const SPLIT = 5;                                    //  How many bit per char
    const POWER = 1;                                    //  How many time decoding/encoding
    const NB_CHAR_PER_SPLIT = 1;
    const BASE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567';    //  Characters to use to obtain the coded string
    const BYTES_BLOCK_LENGTH = 8;                       //  Length of each block

    use BaseEncoderTrait;
}