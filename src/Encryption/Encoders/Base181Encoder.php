<?php
/**
 * Base181Encoder.php
 *
 * @copyright 2021
 * @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 */

namespace Flagstone\EncryptionBundle\Encryption\Encoders;

use Flagstone\EncryptionBundle\Encryption\AbstractBaseEncoder;
use Flagstone\EncryptionBundle\Encryption\BaseEncoderTrait;

/**
 * Class Base181Encoder
 * | Code and decode a string in Base181 format.
 * | A 15 bits length string is coded in a 32 bits string (4 chars). Use 14 different chars for encoding.
 * | Take 3 times more place in database, string must have 10 chars length minimum.
 * @package Flagstone\EncryptionBundle\Encryption\Encoders
 */
class Base181Encoder extends AbstractBaseEncoder
{
    const SPLIT = 15;               //  How many bit per char
    const POWER = 2;                //  How many time decoding/encoding
    const NB_CHAR_PER_SPLIT = 2;
    const BASE = 'abcdefghijklmn';  //  Characters to use to obtain the coded string
    const BYTES_BLOCK_LENGTH = 0;   //  Length of each block

    use BaseEncoderTrait;
}