<?php
/**
 * Base3Encoder.php
 *
 * @copyright 2021
 * @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 */

namespace Flagstone\EncryptionBundle\Encryption\Encoders;

use Flagstone\EncryptionBundle\Encryption\AbstractBaseEncoder;
use Flagstone\EncryptionBundle\Encryption\BaseEncoderTrait;

/**
 * Class Base3Encoder
 * | Code and decode a string in Base3 format.
 * | A 3 bits length string is coded in a 16 bits string (2 chars). Use 3 different chars for encoding.
 * | Take 7 times more place in database
 * @package Flagstone\EncryptionBundle\Encryption\Encoders
 */
class Base3Encoder extends AbstractBaseEncoder
{
    const SPLIT = 3;                //  How many bit per char
    const POWER = 1;                //  How many time decoding/encoding
    const NB_CHAR_PER_SPLIT = 2;
    const BASE = 'abc';             //  Characters to use to obtain the coded string
    const BYTES_BLOCK_LENGTH = 0;   //  Length of each block

    use BaseEncoderTrait;
}