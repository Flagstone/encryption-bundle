<?php
/**
 * Base32HexEncoder.php
 *
 * @copyright 2021
 * @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 */

namespace Flagstone\EncryptionBundle\Encryption\Encoders;

use Flagstone\EncryptionBundle\Encryption\Exceptions\NonUniqueCharacterBaseStringException;

/**
 * Class Base32HexEncoder
 * | Code and decode a string in Base32 format.
 * | Respect RFC 4648 (https://tools.ietf.org/html/rfc4648#section-5) with Extended Hex Alphabet
 * | Take 3 times more place in database
 * @package Flagstone\EncryptionBundle\Encryption\Encoders
 */
class Base32HexEncoder extends Base32Encoder
{
    const BASE = '0123456789ABCDEFGHIJKLMNOPQRSTUV';    //  Characters to use to obtain the coded string

    /**
     *  Base32HexEncoder constructor.
     *  @throws NonUniqueCharacterBaseStringException
     */
    public function __construct()
    {
        parent::__construct();
        $this->changeBaseString(self::BASE);
    }
}