<?php
/**
 * Base1024Encoder.php
 *
 * @copyright 2021
 * @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 */

namespace Flagstone\EncryptionBundle\Encryption\Encoders;

use Flagstone\EncryptionBundle\Encryption\AbstractBaseEncoder;
use Flagstone\EncryptionBundle\Encryption\BaseEncoderTrait;

/**
 * Class Base1024Encoder
 * | Code and decode a string in Base1024 format.
 * | Take 3 times more place in database
 * @package Flagstone\EncryptionBundle\Encryption\Encoders
 */
class Base1024Encoder extends AbstractBaseEncoder
{
    const SPLIT = 20;                               //  How many bit per char
    const POWER = 2;                                //  How many time decoding/encoding
    const NB_CHAR_PER_SPLIT = 2;
    const BASE = 'abcdefghijklmnopqrstuvwxyz234567';//  Characters to use to obtain the coded string
    const BYTES_BLOCK_LENGTH = 0;                   //  Length of each block

    use BaseEncoderTrait;
}