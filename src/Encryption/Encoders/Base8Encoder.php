<?php
/**
 * Base8Encoder.php
 *
 * @copyright 2021
 * @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 */

namespace Flagstone\EncryptionBundle\Encryption\Encoders;

use Flagstone\EncryptionBundle\Encryption\AbstractBaseEncoder;
use Flagstone\EncryptionBundle\Encryption\BaseEncoderTrait;

/**
 * Class Base8Encoder
 * | Code and decode a string in Base8 format.
 * | Take 3 times more place in database
 * @package Flagstone\EncryptionBundle\Encryption\Encoders
 */
class Base8Encoder extends AbstractBaseEncoder
{
    const SPLIT = 3;                //  How many bit per char
    const POWER = 1;                //  How many time decoding/encoding
    const NB_CHAR_PER_SPLIT = 1;
    const BASE = 'abcdefgh';        //  Characters to use to obtain the coded string
    const BYTES_BLOCK_LENGTH = 0;   //  Length of each block

    use BaseEncoderTrait;
}