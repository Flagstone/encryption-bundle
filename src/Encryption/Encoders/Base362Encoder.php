<?php
/**
 * Base362Encoder.php
 *
 * @copyright 2021
 * @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 */

namespace Flagstone\EncryptionBundle\Encryption\Encoders;

use Flagstone\EncryptionBundle\Encryption\AbstractBaseEncoder;
use Flagstone\EncryptionBundle\Encryption\BaseEncoderTrait;

/**
 * Class Base362Encoder
 * | Code and decode a string in Base181 format.
 * | A 17 bits length string is coded in a 32 bits string (4 chars). Use 20 different chars for encoding.
 * @package Flagstone\EncryptionBundle\Encryption\Encoders
 */
class Base362Encoder extends AbstractBaseEncoder
{
    const SPLIT = 17;                   //  How many bit per char
    const POWER = 2;                    //  How many time decoding/encoding
    const NB_CHAR_PER_SPLIT = 2;
    const BASE = 'abcdefghijklmnopqrst';//  Characters to use to obtain the coded string
    const BYTES_BLOCK_LENGTH = 0;       //  Length of each block

    use BaseEncoderTrait;
}