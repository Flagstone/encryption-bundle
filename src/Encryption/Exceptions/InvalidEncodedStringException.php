<?php
/**
 * InvalidEncodedStringException.php
 *
 * @copyright 2021
 * @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 */

namespace Flagstone\EncryptionBundle\Encryption\Exceptions;

use Exception;

/**
 * Class InvalidEncodedStringException
 * @package Flagstone\EncryptionBundle\Encryption\Exceptions
 */
class InvalidEncodedStringException extends Exception
{

}