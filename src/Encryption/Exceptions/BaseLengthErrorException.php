<?php
/**
 * BaseLengthErrorException.php
 *
 * @copyright 2021
 * @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 */

namespace Flagstone\EncryptionBundle\Encryption\Exceptions;

use Exception;

/**
 * Class BaseLengthErrorException
 * @package Flagstone\EncryptionBundle\Encryption\Exceptions
 */
class BaseLengthErrorException extends Exception
{

}