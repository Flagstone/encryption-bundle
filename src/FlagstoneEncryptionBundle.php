<?php
/**
 * FlagstoneEncryptionBundle
 *
 * @copyright 2021
 * @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 */

namespace Flagstone\EncryptionBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class FlagstoneEncryptionBundle extends Bundle
{

}